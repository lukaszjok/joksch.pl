-- Uncomment if You want to drop table
--DROP TABLE joksch_pl.wojewodztwa;
--DROP TABLE joksch_pl.powiaty;
--DROP TABLE joksch_pl.miasta_stolica;
--DROP TABLE joksch_pl.jednostki_terytorialne;
--DROP TRIGGER joksch_pl.jednostki_terytorialne_trg;
--DROP SEQUENCE joksch_pl.jednostki_terytorialne_seq;


-- create additional DB user
CREATE USER joksch_pl IDENTIFIED BY joksch;

-- add admin privileges to own user
GRANT dba TO joksch_pl WITH ADMIN OPTION;

--to remove all data from tables 
TRUNCATE TABLE joksch_pl.wojewodztwa;
TRUNCATE TABLE joksch_pl.powiaty;
TRUNCATE TABLE joksch_pl.miasta_stolica;
TRUNCATE TABLE joksch_pl.jednostki_terytorialne;

CREATE TABLE joksch_pl.wojewodztwa(
woj VARCHAR2(2) NOT NULL,
nazwa VARCHAR2(32),
stan_na DATE,
constraint joksch_pl_wojewodztwa_pk primary key (woj) 
);

CREATE TABLE joksch_pl.powiaty(
woj VARCHAR2(2) NOT NULL,
pow VARCHAR2(2) NOT NULL,
nazwa VARCHAR2(32),
stan_na DATE,
powiat_pelny VARCHAR2(8) NOT NULL,
constraint joksch_pl_powiaty_pk primary key (powiat_pelny) 
);


CREATE TABLE joksch_pl.miasta_stolica(
woj VARCHAR2(2) NOT NULL,
pow VARCHAR2(2) NOT NULL,
nazwa VARCHAR2(32),
nazwa_dod VARCHAR2(40),
stan_na DATE,
powiat_pelny VARCHAR2(8) NOT NULL,
constraint joksch_pl_miasta_stolica_pk primary key (powiat_pelny) 
);


CREATE TABLE joksch_pl.jednostki_terytorialne(
id NUMBER(10) NOT NULL,
woj VARCHAR2(2) NOT NULL,
pow VARCHAR2(2) NOT NULL,
gmi VARCHAR2(2) NOT NULL,
rodz VARCHAR2(2) NOT NULL,
nazwa VARCHAR2(32),
nazwa_dod VARCHAR2(40),
stan_na DATE,
powiat_pelny VARCHAR2(8) NOT NULL,
constraint joksch_pl_jedn_terytorialne_pk primary key (id) 
);

CREATE SEQUENCE joksch_pl.jednostki_terytorialne_seq START WITH 1;

CREATE OR REPLACE TRIGGER joksch_pl.jednostki_terytorialne_trg
BEFORE INSERT ON joksch_pl.jednostki_terytorialne
FOR EACH ROW

BEGIN
  SELECT joksch_pl.jednostki_terytorialne_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/


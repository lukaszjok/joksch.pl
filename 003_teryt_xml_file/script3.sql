INSERT ALL
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('02','61','Jelenia Góra','miasto na prawach powiatu','2018-01-02','0261')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('02','62','Legnica','miasto na prawach powiatu','2018-01-02','0262')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('02','64','Wrocław','miasto na prawach powiatu','2018-01-02','0264')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('02','65','Wałbrzych','miasto na prawach powiatu','2018-01-02','0265')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('04','61','Bydgoszcz','miasto na prawach powiatu','2018-01-02','0461')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('04','62','Grudziądz','miasto na prawach powiatu','2018-01-02','0462')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('04','63','Toruń','miasto na prawach powiatu','2018-01-02','0463')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('04','64','Włocławek','miasto na prawach powiatu','2018-01-02','0464')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('06','61','Biała Podlaska','miasto na prawach powiatu','2018-01-02','0661')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('06','62','Chełm','miasto na prawach powiatu','2018-01-02','0662')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('06','63','Lublin','miasto na prawach powiatu','2018-01-02','0663')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('06','64','Zamość','miasto na prawach powiatu','2018-01-02','0664')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('08','61','Gorzów Wielkopolski','miasto na prawach powiatu','2018-01-02','0861')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('08','62','Zielona Góra','miasto na prawach powiatu','2018-01-02','0862')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('10','61','Łódź','miasto na prawach powiatu','2018-01-02','1061')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('10','62','Piotrków Trybunalski','miasto na prawach powiatu','2018-01-02','1062')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('10','63','Skierniewice','miasto na prawach powiatu','2018-01-02','1063')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('12','61','Kraków','miasto na prawach powiatu','2018-01-02','1261')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('12','62','Nowy Sącz','miasto na prawach powiatu','2018-01-02','1262')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('12','63','Tarnów','miasto na prawach powiatu','2018-01-02','1263')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('14','61','Ostrołęka','miasto na prawach powiatu','2018-01-02','1461')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('14','62','Płock','miasto na prawach powiatu','2018-01-02','1462')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('14','63','Radom','miasto na prawach powiatu','2018-01-02','1463')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('14','64','Siedlce','miasto na prawach powiatu','2018-01-02','1464')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('14','65','Warszawa','miasto stołeczne, na prawach powiatu','2018-01-02','1465')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('16','61','Opole','miasto na prawach powiatu','2018-01-02','1661')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('18','61','Krosno','miasto na prawach powiatu','2018-01-02','1861')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('18','62','Przemyśl','miasto na prawach powiatu','2018-01-02','1862')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('18','63','Rzeszów','miasto na prawach powiatu','2018-01-02','1863')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('18','64','Tarnobrzeg','miasto na prawach powiatu','2018-01-02','1864')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('20','61','Białystok','miasto na prawach powiatu','2018-01-02','2061')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('20','62','Łomża','miasto na prawach powiatu','2018-01-02','2062')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('20','63','Suwałki','miasto na prawach powiatu','2018-01-02','2063')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('22','61','Gdańsk','miasto na prawach powiatu','2018-01-02','2261')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('22','62','Gdynia','miasto na prawach powiatu','2018-01-02','2262')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('22','63','Słupsk','miasto na prawach powiatu','2018-01-02','2263')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('22','64','Sopot','miasto na prawach powiatu','2018-01-02','2264')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','61','Bielsko-Biała','miasto na prawach powiatu','2018-01-02','2461')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','62','Bytom','miasto na prawach powiatu','2018-01-02','2462')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','63','Chorzów','miasto na prawach powiatu','2018-01-02','2463')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','64','Częstochowa','miasto na prawach powiatu','2018-01-02','2464')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','65','Dąbrowa Górnicza','miasto na prawach powiatu','2018-01-02','2465')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','66','Gliwice','miasto na prawach powiatu','2018-01-02','2466')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','67','Jastrzębie-Zdrój','miasto na prawach powiatu','2018-01-02','2467')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','68','Jaworzno','miasto na prawach powiatu','2018-01-02','2468')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','69','Katowice','miasto na prawach powiatu','2018-01-02','2469')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','70','Mysłowice','miasto na prawach powiatu','2018-01-02','2470')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','71','Piekary Śląskie','miasto na prawach powiatu','2018-01-02','2471')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','72','Ruda Śląska','miasto na prawach powiatu','2018-01-02','2472')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','73','Rybnik','miasto na prawach powiatu','2018-01-02','2473')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','74','Siemianowice Śląskie','miasto na prawach powiatu','2018-01-02','2474')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','75','Sosnowiec','miasto na prawach powiatu','2018-01-02','2475')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','76','Świętochłowice','miasto na prawach powiatu','2018-01-02','2476')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','77','Tychy','miasto na prawach powiatu','2018-01-02','2477')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','78','Zabrze','miasto na prawach powiatu','2018-01-02','2478')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('24','79','Żory','miasto na prawach powiatu','2018-01-02','2479')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('26','61','Kielce','miasto na prawach powiatu','2018-01-02','2661')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('28','61','Elbląg','miasto na prawach powiatu','2018-01-02','2861')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('28','62','Olsztyn','miasto na prawach powiatu','2018-01-02','2862')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('30','61','Kalisz','miasto na prawach powiatu','2018-01-02','3061')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('30','62','Konin','miasto na prawach powiatu','2018-01-02','3062')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('30','63','Leszno','miasto na prawach powiatu','2018-01-02','3063')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('30','64','Poznań','miasto na prawach powiatu','2018-01-02','3064')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('32','61','Koszalin','miasto na prawach powiatu','2018-01-02','3261')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('32','62','Szczecin','miasto na prawach powiatu','2018-01-02','3262')
INTO miasta_stolica (woj, pow, nazwa, nazwa_dod, stan_na, powiat_pelny) VALUES ('32','63','Świnoujście','miasto na prawach powiatu','2018-01-02','3263')
SELECT * FROM dual;

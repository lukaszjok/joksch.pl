# Author: Lukasz Joksch
# Source www.joksch.pl
library(caret)
library(randomForest)
#set.seed(123)

#import dataset
dataset <- read.table("D://my_R_projects//Data_Repository//UCI//breast-cancer-wisconsin.data", sep=",")
#remove rows with incomplet data
dataset <- dataset[-which(dataset == "?", arr.ind = TRUE)[,1],]
#Set the columns into data frame (dataset)
attributesNames <- c("Id",
                "Clump_Thickness",
                "Uniformity_of_Cell_Size",
                "Uniformity_of_Cell_Shape",
                "Marginal_Adhesion",
                "Single_Epithelial_Cell_Size",
                "Bare_Nuclei",
                "Bland_Chromatin",
                "Normal_Nucleoli",
                "Mitose",
                "Class")
#Names the columns into data frame (dataset)
names(dataset)<-attributesNames

#Factorization of categorical data (here: features/column)
dataset$Class <- factor(dataset$Class,
                        levels = c(2,4),#Warning: to speed up You can enter by hand the levels!
                        labels = c("benign","malignant")
)

splitTrainingTesting=0.80
splitControl=0.70
trainIndexes <- createDataPartition(dataset$Class, p=splitTrainingTesting, list=FALSE)
trainingData <- dataset[trainIndexes,]
testingData <- dataset[-trainIndexes,]

model<-randomForest(Class~ . , data = trainingData, ntree = 800)
predictions <- predict(model, testingData)

patient1 <- testingData[1,]
patient2 <- testingData[2,]
patient3 <- testingData[5,]

patient1$Class <-NA
patient2$Class <-NA
patient3$Class <-NA


DoctorR <- data.frame()
DoctorR <- rbind(DoctorR,
  c(patient1$Id ,as.character(testingData[1,]$Class), as.character(predict(model, newdata= patient1))),
  c(patient2$Id, as.character(testingData[2,]$Class), as.character(predict(model, patient2))),
  c(patient3$Id, as.character(testingData[5,]$Class), as.character(predict(model, patient3)))
)

names(DoctorR)<-c("Patient_ID", "Real_result_from_downloaded_data","Predicted_data")

View(DoctorR)
print(model)
print("###############")
print(postResample(predictions, testingData$Class))
print("###############")


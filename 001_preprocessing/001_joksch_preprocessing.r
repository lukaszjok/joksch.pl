# Author: Lukasz Joksch
# Source www.joksch.pl

# load the  dataset
dataset <- read.csv(file = 'test_dataset.csv', #file path
                   sep = ",", #separator between columns
                   header = TRUE, #if TRUE file contain row (first) with columns names
                   na.strings = c("brak","","NULL"), #a character vector of strings 
                                                     #which will be interpreted as NA values
                   stringsAsFactors = FALSE #if TRUE convert stings as FACTORS
                   )

names(dataset) <- c("job_name","age","salary","paid") #create columns names

#Create columns names by sequence with pattern 
#It is optional, UNCOMMENT if You want (when You have not names of columns)
#names(dataset) <- paste("Feature",seq(1:ncol(dataset)),sep = "_")

naSum <- sum(is.na(dataset))


###--------------------------------------------------------------------------------------------------
# Taking care of missing data - replacing NA(null) values by enter column (feature) name of dataset

#Make cell as NA base on string pattern
NAindicators <- c("\\?","unknown")
for(NApattern in NAindicators)(
  dataset <- data.frame(sapply(dataset, function(x) gsub(NApattern, NA, x)))
)

# NA's statistics
statNaCount <- colSums(is.na(dataset))
statRowsWithNa <- dataset[!complete.cases(dataset),]
statRowsWithNaCount <- nrow(statRowsWithNa)

print("Counter NA's in each column ")
print(statNaCount)
print("=============================================================")
print("Show complete rows with NA's values in any column ")
print(statRowsWithNa)
print("=============================================================")
print("Number of rows with any NA's values")
print(statRowsWithNaCount)
print("=============================================================")

#columns with incomplete data
columnsNamesToEnterData <- c( "job_name","salary")

columnsPositionToEnterData <- numeric()
featuresNames <- names(dataset)
for(columnName in columnsNamesToEnterData ){
  columnsPositionToEnterData <- append(columnsPositionToEnterData,which(featuresNames == columnName))
}

for(columnPosition in columnsPositionToEnterData ){
  #Here we can use different strategy: mean, median, sum etc. in "FUN" parameter
  dataset[,columnPosition] = ifelse(is.na(dataset[,columnPosition]),
                                    ave(dataset[,columnPosition], FUN = function(x) mean(x, na.rm = TRUE)),
                                    dataset[,columnPosition])
}

#make decision what to do with lost data
dataset<-na.omit(dataset)


statNaCountAfterNaHandle <- colSums(is.na(dataset))
print("Counter NA's in each column ")
print(statNaCountAfterNaHandle)
###--------------------------------------------------------------------------------------------------
#Factorization of categorical data (here: features/column)
dataset$paid<- factor(dataset$paid,
                            levels = levels(dataset$paid),#Warning: to speed up You can enter by hand the levels!
                            labels = c(0,1)
)

###------------------------------------------------------------------------------------------------
# Set datatype on clear dataset

dataset$job_name <- as.character(dataset$job_name)
dataset$age <- as.numeric(as.character(dataset$age)) #Note on correct syntax while conversion from character to numeric
dataset$salary <- as.numeric(as.character(dataset$salary)) #Note on correct syntax while conversion from character to numeric
dataset$paid<- as.factor(dataset$paid)

###------------------------------------------------------------------------------------------------
# Values normalization

#Here we normalize column age and salary
#We use R�s built-in scale() function - Z-SCORE normalization
dataset[2:3] <- scale(dataset[2:3])

View(dataset)

#save and load dataset object with original variable name "dataset"
#You are not able to change variable name
save(dataset, "file1.rda")
load(file = "file1.rda")

#save and load dataset object with new variable name "new_dataset"
saveRDS(dataset, "file2.rds")
newDataset<- readRDS(file = "file2.rds")

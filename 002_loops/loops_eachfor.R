# Author: Lukasz Joksch
# Source www.joksch.pl

liczby <- c("jeden", "dwa", "trzy", "cztery", "piec")
zwierzeta <- c("pies","kot","mysz","ryba")
cyferki <- c(1,2,3,4)
wektor <- c(liczby,zwierzeta,cyferki)
dane <- data.frame(zwierzeta,cyferki)

print("---LICZBY---")
for(obieg in liczby)
{
  print(obieg)
}

print("---ZWIERZETA---")
for(obieg in zwierzeta)
{
  print(obieg)
}

print("---CYFERKI---")
for(obieg in cyferki)
{
  print(obieg)
}

print("---WEKTOR---")
for(obieg in wektor)
{
  print(obieg)
}

print("---DANE---")
for(obieg in dane)
{
  print(obieg)
}